package fr.lowden.core.commons.db.redis;

import redis.clients.jedis.JedisPool;

public class RedisManager {
    JedisPool pool;

    String host;
    int port;
    String password;

    public RedisManager(String host, int port, String password) {
        this.host = host;
        this.port = port;
        this.password = password;

        pool = new JedisPool(host, port);
    }

    public JedisPool getPool() {
        return pool;
    }

    public String getString(String key){
        return "";
    }

    public int getInt(String key){
        return 0;
    }

    public void set(){}
}
