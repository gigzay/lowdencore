package fr.lowden.core.bukkit;


import fr.lowden.api.spectroworldapi.SpectroWorldApi;

import fr.lowden.core.commons.db.redis.RedisManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

public class LowdenSpigotCore  extends JavaPlugin implements SpectroWorldApi {



    ConsoleCommandSender console = getServer().getConsoleSender();

    RedisManager redisManager;


    @Override
    public void onLoad() {
        console.sendMessage(ChatColor.LIGHT_PURPLE + "Loading LowdenCore");
        saveDefaultConfig();

        //Redis Initialisation
        redisManager = new RedisManager(getConfig().getString("redis.host"), getConfig().getInt("redis.port"), getConfig().getString("redis.pass"));
    }

    @Override
    public void onEnable() {


        console.sendMessage(ChatColor.GREEN + "Successful loading LowdenCore, trying to connect to databases...");
    }

    @Override
    public void onDisable() {
        console.sendMessage(ChatColor.RED + "Sucessful Unloading LowdenCore !");
    }


}
